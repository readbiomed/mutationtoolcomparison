
The folder "annotations" contains the genes.tgz file with the gene annotation of the different source files and the mutations.tgz file with the mutations annotated by each tool on each data source. 

InSiGHT database accessed on 02 January 2013 to establish our data set. Our data set only has curated mutations for four genes: MLH1, MSH2, MSH6 and PMS2.

COSMIC version v62 (from 29th of November 2012) available from the COSMIC FTP site (ftp://ftp.sanger.ac.uk/pub/CGP/cosmic), including mutation information curated from 9,950 unique PubMed articles.

Each line has information about one curated mutation.

Field 1: PMID (PubMed Identifier) of the article
Field 2: Gene name as used in the database
Field 3: cDNA Mutation in HGVS format
Field 4: Protein Mutation in HGVS format
Field 5: Gene identifier in the NCBI gene database (http://www.ncbi.nlm.nih.gov/gene)

An example from the COSMIC database is shown below:

18561205|MLH1|c.1038G>C|p.Q346H|4292
