Variome Corpus mutation subset
==============================

Each folder starting with "annotation_" contains the annotation files (*.ann) in brat format (http://brat.nlplab.org/standoff.html) required for the evaluation of either all mutations, DNA mutations or protein mutations.

The folder tool_annotations contains the ann files produced using each one of the evaluated tools.

Evaluation of brat annotated data sets can be done with the brateval tool:

https://bitbucket.org/nicta_biomed/brateval

Brateval has been used to evaluate several mutatation extraction tools:

Antonio Jimeno Yepes and Karin Verspoor
Mutation extraction tools can be combined for robust recognition of genetic variants in the literature
F1000Research 3, 2014