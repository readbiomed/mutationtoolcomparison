MutationToolComparison
----------------------

This project contains code and data used in the evaluation of several state of the art mutation extraction tools.


- src - contains Java code used in the development

  Packages to be installed before using the java classes:

    * Apache Tika: http://tika.apache.org
    * Apache UIMA: http://uima.apache.org
    * ccp-nlp: http://bionlp-uima.sourceforge.net, it might require uimaFIT from https://code.google.com/p/uimafit (not the newest version from Apache)
    * BratEval: https://bitbucket.org/nicta_biomed/brateval
    * monqJfa: http://monqjfa.berlios.de
    * Scala: http://scala-lang.org
    * SETH: http://rockt.github.io/SETH

- data - data used in the comparison of the algorithms
\ -- databases - benchmark for evaluation of mutations extracted from articles
\ -- Variome_corpus - data set used for the comparison of mutation algorithms versus the Variome corpus [1]


The results using this data and code have been published in:

Jimeno Yepes, Antonio, and Karin Verspoor. "Mutation extraction tools can be combined for robust recognition of genetic variants
in the literature." F1000Research 3:18 (2014).
http://f1000research.com/articles/3-18/v2


References:

[1] Karin Verspoor, Antonio Jimeno Yepes, Lawrence Cavedon, Tara McIntosh, Asha Herten-Crabb, Zoe Thomas, John-Paul Plazzer (2013)
Annotating the Biomedical Literature for the Human Variome.
Database: The Journal of Biological Databases and Curation, virtual issue for BioCuration 2013 meeting. 2013:bat019.
doi:10.1093/database/bat019