package readbiomed.mtc.annotators;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import au.com.nicta.csp.brateval.Annotations;
import au.com.nicta.csp.brateval.Document;
import au.com.nicta.csp.brateval.Entity;
import au.com.nicta.csp.brateval.Location;

/**
 * Merging of mutation annotation in brat format.
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class MergeMutationAnnotation
{
  // Change this list to your list of annotations
  private static String [] folders =
	  {
	    "C:\\...\\MutationFinder",
	    "C:\\...\\tmVar\\ann",
	    "C:\\...\\seth",
        "C:\\...\\EMU",
        "C:\\...\\MutationMiner"
	  };

  /**
   * Example call: MergeMutationAnnotation [output_folder]
   * Change the set of folders considered for 
   * 
   * @param argc 
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
    String output_folder = argc[0];
	  
	// Get file names
	File lf = new File(folders[0]);

	System.out.println(lf.listFiles().length);

	for (File f : lf.listFiles())
	{
	  System.out.println(f.getName());
	  Document d = null;

	  int count = 1;

      for (String folder : folders)
      {
        Document d1 = Annotations.read(folder + "\\" + f.getName(), "ann");

        if (d == null)
        { d = d1; }
        else
        {
          for (Entity e : d1.getEntities())
          {
        	Entity e1 = d.findEntityOverlapNoType(e);

        	if (e1 == null)
        	{
        	  d.addEntity("T" + count, new Entity ("T" + count, e.getType(), e.getLocations(), e.getString(), "ann"));
        	  count++;
        	}
        	else
        	{
              Location l =
            		  new Location((e1.getLocations().get(0).getStart() < e.getLocations().get(0).getStart() ? e1.getLocations().get(0).getStart() : e.getLocations().get(0).getStart()),
            		               (e1.getLocations().get(0).getEnd() > e.getLocations().get(0).getEnd() ? e1.getLocations().get(0).getEnd() : e.getLocations().get(0).getEnd())
            		  );

              LinkedList <Location> ll = new LinkedList <Location> ();
              ll.add(l);

              d.addEntity(e1.getId(), new Entity (e1.getId(),
            		                              e.getType(),
            		                              ll,
            		                              e.getString(),
            		                              "ann"));
        	}
          }
        }
      }

      Annotations.write(output_folder + "\\" + f.getName(), d);
    }
  }
}