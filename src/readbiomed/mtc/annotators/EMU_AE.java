package readbiomed.mtc.annotators;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.resource.ResourceInitializationException;

import au.com.nicta.csp.brateval.Document;
import au.com.nicta.csp.brateval.Entity;
import au.com.nicta.csp.brateval.Location;
import edu.ucdenver.ccp.nlp.core.annotation.AnnotationSet;
import edu.ucdenver.ccp.nlp.core.annotation.Annotator;
import edu.ucdenver.ccp.nlp.core.annotation.TextAnnotation;
import edu.ucdenver.ccp.nlp.core.annotation.impl.DefaultTextAnnotation;
import edu.ucdenver.ccp.nlp.core.exception.InitializationException;
import edu.ucdenver.ccp.nlp.core.interfaces.IEntityTagger;
import edu.ucdenver.ccp.nlp.core.interfaces.ITagger;
import edu.ucdenver.ccp.nlp.core.mention.ClassMention;
import edu.ucdenver.ccp.nlp.core.mention.impl.DefaultClassMention;
import edu.ucdenver.ccp.nlp.uima.annotators.entitydetection.EntityTagger_AE;

public class EMU_AE extends EntityTagger_AE implements IEntityTagger
{
  private final Annotator annotator = new Annotator(1, "emu", "REGEX", "NICTA");
  
  private static Pattern p = Pattern.compile("\t");
  
  private static Map <String, String> amino_acid_three_to_one_map = null; 

  // Extend EntityTagger_AE
  @Override
  public void initialize(UimaContext context)
  throws ResourceInitializationException
  {
	super.initialize(context);
	entityTagger = new EMU_AE();
	
	amino_acid_three_to_one_map = new HashMap<String, String>();
    amino_acid_three_to_one_map = new HashMap<String, String>();
    amino_acid_three_to_one_map.put("ALA", "A");
    amino_acid_three_to_one_map.put("GLY", "G");
    amino_acid_three_to_one_map.put("LEU", "L");
    amino_acid_three_to_one_map.put("MET", "M");
    amino_acid_three_to_one_map.put("PHE", "F");
    amino_acid_three_to_one_map.put("TRP", "W");
    amino_acid_three_to_one_map.put("LYS", "K");
    amino_acid_three_to_one_map.put("GLN", "Q");
    amino_acid_three_to_one_map.put("GLU", "E");
    amino_acid_three_to_one_map.put("SER", "S");
    amino_acid_three_to_one_map.put("PRO", "P");
    amino_acid_three_to_one_map.put("VAL", "V");
    amino_acid_three_to_one_map.put("ILE", "I");
    amino_acid_three_to_one_map.put("CYS", "C");
    amino_acid_three_to_one_map.put("TYR", "Y");
    amino_acid_three_to_one_map.put("HIS", "H");
    amino_acid_three_to_one_map.put("ARG", "R");
    amino_acid_three_to_one_map.put("ASN", "N");
    amino_acid_three_to_one_map.put("ASP", "D");
    amino_acid_three_to_one_map.put("THR", "T");
	
	try
	{ entityTagger.initialize(ITagger.ENTITY_TAGGER, null); }
	catch (InitializationException e)
	{ throw new ResourceInitializationException(e);	}
  }

  @Override
  public void initialize(int taggerType, String[] args)
  throws InitializationException
  { }

  @Override
  public List<TextAnnotation> getEntitiesFromText(String inputText,	String documentID)
  {
	List <TextAnnotation> entityAnnotations = new ArrayList <TextAnnotation> ();

	// Configure your proxy in case you need it
	String [] env = { "HTTP_proxy=..." };
	// Set the folder for EMU
	String folder_name = "C:\\...\\EMU";
	String file_name = String.valueOf(UUID.randomUUID());
	String emu_file_name = "EMU_1.19_HUGO_" + file_name;
	//String emu_seq_file_name = "EMU_1.19_HUGO_seq_" + file_name;
	// Set your perl path
	String perl_path = "C:\\Perl64\\bin\\";

    try
    {
      // Write the file
      BufferedWriter w = new BufferedWriter(new FileWriter(new File(folder_name, file_name)));
      w.write("pmid\ttitle\tabstract");
      w.newLine();
      
      {
        BufferedReader b = new BufferedReader(new StringReader(inputText));

        String line;
        int line_count = 1;

        while ((line = b.readLine()) != null)
        {
          w.write(documentID + "_" + line_count);
          w.write("\t\t");
          w.write(line);
          w.newLine();

          line_count++;
        }

        b.close();
      }

      w.flush();
      w.close();

      // Run EMU
      runEMU(perl_path, folder_name, file_name, env);
      //runEMUSeq(perl_path, folder_name, file_name, env, emu_file_name, emu_seq_file_name);

      // Retrieve the annotations
      BufferedReader b = new BufferedReader(new FileReader(new File(folder_name, emu_file_name)));
      String line;

      int count = 1;

      System.out.println(new File(folder_name, emu_file_name));
      
      System.out.println(inputText);

      //Remove first line
      if (b.readLine() != null)
      {
    	Document d = new Document ();

        // Collect entities
        while ((line = b.readLine()) != null)
        {
          String [] tokens = p.split(line);

          int previous_pos = 0;

          System.out.println(line);

          while (inputText.indexOf(tokens[2], previous_pos) > -1)
          {
        	System.out.println(inputText.indexOf(tokens[2], previous_pos) + "|" + tokens[2] + previous_pos);
        	  
            int start = inputText.indexOf(tokens[2], previous_pos);
            int end = start + tokens[2].length();

            previous_pos = end;

            LinkedList <Location> l = new LinkedList <Location> ();
            l.add(new Location(start, end));
            
        	Entity e = new Entity("T" + count, "mutation", l, tokens[2].substring(1,tokens[2].length()-2), "ann");

        	if (d.findEntity(e) == null)
        	{
              d.addEntity(e.getId(), e);
              count++;

  	          TextAnnotation ta = new DefaultTextAnnotation(start + 1, end - 1);
    	      ta.setAnnotator(annotator);
    	      ta.setDocumentID(documentID);
    	      ta.setCoveredText(tokens[2].substring(1,tokens[2].length()-2));
    	      ta.setAnnotationComment(line);
    	      ta.addAnnotationSet(new AnnotationSet());

    	      ClassMention cm;
    	      cm = new DefaultClassMention("mutation");
    	      ta.setClassMention(cm);

    	      entityAnnotations.add(ta);

    	      String wtaa =
                (amino_acid_three_to_one_map.get(tokens[5]) == null ? tokens[5] : amino_acid_three_to_one_map.get(tokens[5]));
    	      String mtaa =
                (amino_acid_three_to_one_map.get(tokens[6]) == null ? tokens[6] : amino_acid_three_to_one_map.get(tokens[6]));

  	          System.out.println(documentID + "|" + wtaa + tokens[7] + mtaa);
        	}
          }
        }
      }

      b.close();
	}
    catch (IOException e)
    { e.printStackTrace(); }
    finally
    {
      // Remove the files
      new File(folder_name, file_name).delete();
      new File(folder_name, emu_file_name).delete();
      //new File(folder_name, emu_seq_file_name).delete();
    }

	return entityAnnotations;
  }

  public void shutdown()
  { }

  protected static void runEMU(String perl_path, String folder_name, String file_name, String [] env)
  throws IOException  
  {
    Process p = Runtime.getRuntime().exec(perl_path + "perl EMUv1.0.19.pl -f " + file_name, env, new File (folder_name));

    BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
    while (b.readLine() != null);
    b.close();

    p.destroy();
  }

  private static void runEMUSeq(String perl_path, String folder_name, String file_name, String [] env, String emu_file_name, String emu_seq_file_name)
  throws IOException  
  {
    Process p = Runtime.getRuntime().exec(perl_path + "perl EMU_seq_filter_v1.2.pl " + emu_file_name + " " + emu_seq_file_name, env, new File (folder_name));

    BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
    while (b.readLine() != null);
    b.close();

    p.destroy();
  }
}