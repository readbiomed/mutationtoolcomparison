package readbiomed.mtc.annotators;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import scala.collection.Iterator;
import scala.collection.immutable.List;
import seth.ner.Mutation;
import seth.ner.SETHNER;

/**
 * SETH wrapper (http://rockt.github.io/SETH)
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class Seth
{
  private static String readFile (String file_name)
  throws FileNotFoundException
  {
	String text = null;
	Scanner scanner = null; 

	try
	{
	  scanner = new Scanner(new File(file_name), "UTF-8");
	  text =  scanner.useDelimiter("\\A").next();
	}
	finally
	{ scanner.close(); }

    return text;
  }

  public static void annotateFile(String file_name, String output_file_name)
  throws IOException
  {
    SETHNER seth = new SETHNER(false);

	// Load file into a String and run SETH
    List <Mutation> ms = seth.extractMutations(readFile(file_name));

	// Write output in BRAT format
    BufferedWriter w = new BufferedWriter(new FileWriter(output_file_name));

    int entity_id = 1;

    Iterator <Mutation> i = ms.iterator();

    while (i.hasNext())
    {
      Mutation m = i.next();
      w.write("T" + entity_id + "\tmutation " + m.start() + " " + m.end() + "\t" + m.text());
      w.newLine();
      entity_id++;
    }

    w.flush();
    w.close();
  }

  /**
   * Example call: Seth [data_folder_name] [output_folder_name] 
   * 
   * @param argc
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
	if (argc.length != 2)
	{
	  System.err.println("Seth [data_folder_name] [output_folder_name]");
      System.exit(-1);
	}
	
	File dir = new File(argc[0]);

	if (dir.listFiles() != null)
	{
	  for (File f : dir.listFiles())
	  {
		if (f.getName().endsWith(".txt"))
        {
	      annotateFile(f.getAbsolutePath(),
			           argc[1] + File.separator + f.getName().replaceAll(".txt$", ".ann"));
		}
	  }
	}
  }
}