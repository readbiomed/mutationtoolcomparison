package readbiomed.mtc.annotators.tmVar;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Conversion brat files into files to be used by tmVar
 * 
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class brat2tmvar
{
  private static String readFile (String file_name)
  throws FileNotFoundException
  {
	String text = null;
	Scanner scanner = null; 

	try
	{
	  scanner = new Scanner(new File(file_name), "UTF-8");
	  text =  scanner.useDelimiter("\\A").next();
	}
	finally
	{ scanner.close(); }

	return text;
  }
	
  private static void outputFile(String folder_name, String file_name, int start, String text)
  throws IOException
  {
	File file = new File(folder_name, file_name + "_" + start);

	BufferedWriter w = new BufferedWriter(new FileWriter(file));
	w.write("1|t|" + text);
	w.flush();
	w.close();
  }

  /**
   * Example call: brat2tmvar [brat_folder] [tmvar_output_folder]
   * 
   * @param argc
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
	if (argc.length != 2)
	{
	  System.err.println("brat2tmvar [brat_folder] [tmvar_output_folder]");
	  System.exit(-1);
	}
	  
	String output_folder = argc[1];
    String files_folder = argc[0];

    File folder = new File(files_folder);

    if (folder.isDirectory() && folder.listFiles() != null)
    {
      for (File file : folder.listFiles())
      {
    	if (file.getName().endsWith(".txt"))
    	{
          // Load the text file
          String text = readFile(file.getAbsolutePath());

	      // Split it into paragraphs
          Pattern p = Pattern.compile("\n");

          Matcher m = p.matcher(text);
          int start = 0;

          while (m.find())
          {
            String paragraph = text.substring(start, m.start());
            if (paragraph.trim().length() > 0)
            { outputFile(output_folder, file.getName(), start, text.substring(start, m.start())); }
            start = m.end();
          }

          if (start < text.length())
          {
            String paragraph = text.substring(start, m.start());
            if (paragraph.trim().length() > 0)
   	        { outputFile(output_folder, file.getName(), start, text.substring(start)); }
          }
    	}
      }
    }
  }
}