package readbiomed.mtc.annotators.tmVar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Pattern;

import au.com.nicta.csp.brateval.Annotations;
import au.com.nicta.csp.brateval.Document;
import au.com.nicta.csp.brateval.Entity;
import au.com.nicta.csp.brateval.Location;

/**
 * Conversion from tmVar output format to brat
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class tmVar2Brat
{
  private static Pattern p = Pattern.compile("_");
  private static Pattern p_tab = Pattern.compile("\t");

  /**
   * Example call: tmVar2Brat [tmvar_input_folder] [brat_output_folder]
   * 
   * @param argc - tmvar_input_folder and brat_output_folder
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
	if (argc.length != 2)
	{
	  System.out.println("tmVar2Brat [tmvar_input_folder] [brat_output_folder]");
	  System.exit(-1);
	}
	  
	String tmVar_folder = argc[0];
	String tmVar_ann_folder = argc[1];

	File folder = new File(tmVar_folder);

	Map <String, Document> file_entities =
			new HashMap <String, Document> ();

	Map <String, Integer> file_id =
			new HashMap <String, Integer> ();

	if (folder.isDirectory() && folder.listFiles() != null)
    {
	  for (File file : folder.listFiles())
	  {
        if (file.getName().endsWith(".PubTator"))
        {
		  // Split the name and the position
		  String [] split_name = p.split(file.getName().replaceAll(".PubTator", ""));
		  String name = split_name[0];
		  int offset = Integer.parseInt(split_name[1]);

		  BufferedReader b = new BufferedReader(new FileReader(file));

		  String line;

		  while ((line = b.readLine()) != null)
		  {
		    String [] tokens = p_tab.split(line);

		    if (tokens.length > 3)
		    {
              int start = Integer.parseInt(tokens[1]) + offset;
              int end = Integer.parseInt(tokens[2]) + offset;

              String string = tokens[3];

              // Get the entity id
              Integer id = file_id.get(name);
            
              if (id == null)
              {
                id = 1;
                file_id.put(name,  id);
              }
              else
              {
                id = id + 1;
                file_id.put(name,  id);
              }

              Location l = new Location(start, end);
              LinkedList <Location> ll = new LinkedList <Location> ();
              ll.add(l);
              
              Entity e = new Entity("T" + id, "mutation", ll, string, "ann");

              Document d = file_entities.get(name);

              if (d == null)
              {
                d = new Document();
                file_entities.put(name, d);
              }

              d.addEntity(e.getId(), e);
		    }
		  }

		  b.close();
        }
	  }
    }

	// Once we have all the files, prepare documents and write them
	for (Map.Entry <String, Document> fd : file_entities.entrySet())
	{
	  Document d = file_entities.get(fd.getKey());
      Annotations.write(new File(tmVar_ann_folder, fd.getKey()).getAbsolutePath().replaceAll(".txt", ".ann"), d);
	}
  }
}