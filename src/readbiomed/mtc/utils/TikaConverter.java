package readbiomed.mtc.utils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Conversion of different file formats into text based on Tika (http://tika.apache.org/)
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class TikaConverter
{
  /**
   * Example call: TikaConverter [input_folder] [output_folder]
   * 
   * @param argc - input folder with files to convert and output folder
   * @throws IOException
   * @throws SAXException
   * @throws TikaException
   */
  public static void main (String [] argc)
  throws IOException, SAXException, TikaException
  {
	if (argc.length != 2)
	{
	  System.err.println("TikaConverter [input_folder] [output_folder]");
      System.exit(-1);
	}

	File dir = new File(argc[0]);

	if (dir.isDirectory() && dir.listFiles() != null)
	{
	  for (File file : dir.listFiles())
	  {
		try
		{
	      Parser parser = new AutoDetectParser(new DefaultDetector());
	      Metadata metadata = new Metadata();
	      ParseContext context = new ParseContext();
	      context.set(Parser.class, parser);
	      OutputStream outputstream = new ByteArrayOutputStream();
	      InputStream input = TikaInputStream.get(file, metadata);  
	      ContentHandler handler = new BodyContentHandler(outputstream);
	      parser.parse(input, handler, metadata, context);  
	      input.close();

	      BufferedWriter w = new BufferedWriter(new FileWriter(new File(argc[1], file.getName().replaceAll(".pdf", "") + ".txt")));
	      w.write(outputstream.toString());
	      w.close();
		}
		catch (Exception e)
		{
          System.err.println(file.getName());
		  e.printStackTrace();
		}
	  }
	}
  }
}