package readbiomed.mtc.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import au.com.nicta.csp.brateval.Annotations;
import au.com.nicta.csp.brateval.Document;
import au.com.nicta.csp.brateval.Entity;

/**
 * Calculates the overlap at the character level between annotation sets. 
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class CharacterOverlap
{
	
  /**
   * Example call: CharacterOverlap [folder1] [folder2]
   * 
   * @param argc - path of the folders being compared 
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
	String folder1 = argc[0];
	String folder2 = argc[1];

	Set <String> entityTypes = new TreeSet <String> ();

    File folder = new File(folder1);
    
    int count_total = 0;
    int count_matched = 0;

    for (File file : folder.listFiles())
    {
      if (file.getName().endsWith(".ann"))
      {
        Document d1 = Annotations.read(file.getAbsolutePath(), "ann");
        Document d2 = Annotations.read(folder2 + File.separator +  file.getName(), "ann");

    	for (Entity e : d1.getEntities())
    	{
          entityTypes.add(e.getType());

          Entity match = null;

          match = d2.findEntityOverlapC(e);

          if (match != null)
    	  {
        	count_total += e.getLocations().get(0).getEnd() - e.getLocations().get(0).getStart();
        	// Start of the entity
            int start = (e.getLocations().get(0).getStart() > match.getLocations().get(0).getStart() ? e.getLocations().get(0).getStart() : match.getLocations().get(0).getStart());
        	// End of the entity
            int end = (e.getLocations().get(0).getEnd() < match.getLocations().get(0).getEnd() ? e.getLocations().get(0).getEnd() : match.getLocations().get(0).getEnd());
        	// Total number of matched chars
        	count_matched += end - start;
        	
        	System.out.println(match.getString() + "|" + e.getString() + "|" + count_matched);
    	  }
    	}
      }
    }
    
    System.out.println("Total chars|" + count_total + "|Matched|" + count_matched + "|Overlap|" + String.format("%1.4f", (double)count_matched/count_total));
  }
}