package readbiomed.mtc.evaluation;

public class VariationAnnotation
{
  private String gene_name = null;
  private String gene_id = null;
  private String dna_variation = null;
  private String protein_variation = null;
  
  public String getGeneName()
  { return gene_name; }

  public void setGeneName(String gene_name)
  { this.gene_name = gene_name; }
	
  public String getGeneId()
  { return gene_id; }

  public void setGeneId(String gene_id)
  { this.gene_id = gene_id; }

  public String getDNAVariation()
  { return dna_variation; }

  public void setDNAVariation(String dna_variation)
  { this.dna_variation = dna_variation; }

  public String getProteinVariation()
  { return protein_variation; }

  public void setProteinVariation(String protein_variation)
  { this.protein_variation = protein_variation; }
  
  public String toString()
  {
    return gene_name + "|" + gene_id + "|" + dna_variation + "|" + protein_variation;
  }
}
