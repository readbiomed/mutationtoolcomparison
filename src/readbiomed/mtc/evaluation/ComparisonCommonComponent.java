package readbiomed.mtc.evaluation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class ComparisonCommonComponent
{
  private static Pattern p_pipe = Pattern.compile("\\|");

  private static String normalizeProteinMutation(String protein_mutation)
  {
	return protein_mutation.replaceAll("undefined", "x").replaceAll("\\*", "x");
  }
  
  private static Map <String, Set <String>> readGenes(String gene_file_name, Set <String> pmids)
  throws IOException
  {
	Map <String, Set <String>> pmid_gene = new HashMap <String, Set <String>> ();

	String line;
			
    BufferedReader b = new BufferedReader(new FileReader (gene_file_name));
    
    while ((line = b.readLine()) != null)
    {
      String [] tokens = p_pipe.split(line);
      
      if (pmids.contains(tokens[0]))
      {
    	Set <String> genes = pmid_gene.get(tokens[0]);
    	
    	if (genes == null)
    	{
          genes = new HashSet<String> ();
          pmid_gene.put(tokens[0], genes);
    	}
    	
    	genes.add(tokens[3]);
      }
    }

	b.close();
	
	return pmid_gene;
  }
  
  private static Map <String, Map <String, Set <VariationAnnotation>>> readAnnotations(String variants_file_name, Set <String> pmids)
  throws IOException
  {
	Map <String, Map <String, Set <VariationAnnotation>>> variations =
			new HashMap <String, Map <String, Set <VariationAnnotation>>> ();
	
    // Read the annotation set
    BufferedReader b = new BufferedReader(new FileReader (variants_file_name));
    
    String line;

    while ((line = b.readLine()) != null)
    {
      String [] tokens = p_pipe.split(line);

      if (pmids.contains(tokens[0]))
      {
    	// Same id - remove
    	//tokens[0] = "1";
    	  
    	String gene_id = "1";

        Map <String, Set <VariationAnnotation>> v = variations.get(tokens[0]);

        if (v == null)
        {
          v = new HashMap <String, Set <VariationAnnotation>> ();
          variations.put(tokens[0], v);
        }

        Set <VariationAnnotation> vas = v.get(tokens[2].toLowerCase());

        if (vas == null)
        {
          vas = new HashSet <VariationAnnotation> ();
          v.put(tokens[2].toLowerCase(), vas);
        }

        //for (String gene_id : gene_ids)
        {
          VariationAnnotation va = new VariationAnnotation ();
          va.setGeneName(gene_id);
          va.setDNAVariation(tokens[2].toLowerCase());

          vas.add(va);
          
          if (tokens.length> 3)
          {
        	String protein_mutation = normalizeProteinMutation(tokens[3].toLowerCase()); 

        	va.setProteinVariation(protein_mutation); 

            Set <VariationAnnotation> vasp = v.get(protein_mutation);
            if (vasp == null)
            {
              vasp = new HashSet <VariationAnnotation> ();
              v.put(protein_mutation, vasp);
            }

            vasp.add(va);
          }
        }
      }
    }

    b.close();
    
    return variations;
  }

  public static void main (String [] argc) throws IOException
  {
	Set <String> pmids = new HashSet <String> ();
	
	String annotation_set = argc[0];
    String reference_set = argc[1];
    String gene_set = argc[2];

    String pmids_set = argc[3];

    {
      BufferedReader b = new BufferedReader(new FileReader (pmids_set));
      pmids.clear();
      String line;
      while ((line = b.readLine()) != null)
      { pmids.add(line.trim()); }

      b.close();
    }
    
    Map <String, Set <String>> pmid_gene = readGenes(gene_set, pmids);

    Map <String, Map <String, Set <VariationAnnotation>>> annotation_variants =
    		readAnnotations(annotation_set, pmids);

    // Read the reference set and check if the variations are in the annotation set
    BufferedReader b = new BufferedReader(new FileReader (reference_set));

    String line;
    
    int total_variants = 0;
    int total_variants_common = 0;
    int gene_variants_matched = 0;
    int variants_matched = 0;

    Set <String> common_pmids = new HashSet <String> ();

    while ((line = b.readLine()) != null)
    {
      String [] tokens = p_pipe.split(line);
      
      if (pmids.contains(tokens[0]))
      {

        Map <String, Set <VariationAnnotation>> v = annotation_variants.get(tokens[0]);

        if (v != null)
        {
    	  common_pmids.add(tokens[0]);

    	  //String gene_name = tokens[1];
    	  String gene_name = tokens[tokens.length-1];
          String dna_variant = tokens[2];
          String protein_variant = normalizeProteinMutation((tokens.length > 3 ? tokens[3] : ""));
          
          // Gene + variant
          {
            boolean found = false;

            if (dna_variant.length() > 0 && v.get(dna_variant.toLowerCase()) != null)
            {
              for (VariationAnnotation va : v.get(dna_variant.toLowerCase()))
              {
        	    if (va.getDNAVariation().toLowerCase().equals(dna_variant.toLowerCase()) && pmid_gene.get(tokens[0]) != null && pmid_gene.get(tokens[0]).contains(gene_name.toLowerCase()))
                { gene_variants_matched++; found = true; break; } 
              }
            }

            if (!found && protein_variant.length() > 0 && v.get(protein_variant.toLowerCase()) != null)
            {
              for (VariationAnnotation va : v.get(protein_variant.toLowerCase()))
              {
       		    if (va.getProteinVariation() != null && va.getProteinVariation().toLowerCase().equals(protein_variant.toLowerCase()) && pmid_gene.get(tokens[0]) != null && pmid_gene.get(tokens[0]).contains(gene_name.toLowerCase()))
                { gene_variants_matched++; found = true; break; }
              }
            }
          }

          // Variant
          {
            boolean found = false;

            if (dna_variant.length() > 0 && v.get(dna_variant.toLowerCase()) != null)
            {
              for (VariationAnnotation va : v.get(dna_variant.toLowerCase()))
              {
        	    if (va.getDNAVariation().toLowerCase().equals(dna_variant.toLowerCase()))
        	    {
                  variants_matched++; found = true; break;
        	    }
              }
            }

            if (!found && protein_variant.length() > 0 && v.get(protein_variant.toLowerCase()) != null)
            {
              for (VariationAnnotation va : v.get(protein_variant.toLowerCase()))
              {
        	    if (va.getProteinVariation() != null && va.getProteinVariation().toLowerCase().equals(protein_variant.toLowerCase()))
        	    { variants_matched++; found = true; break; }
              }
            }
          }
          total_variants_common++;
        }
        
        total_variants++;
      }
    }

    b.close();

    {
      double recall = (double)gene_variants_matched/total_variants;
      double recall_variants = (double)variants_matched/total_variants;

      System.out.print(argc[4]);
      System.out.print("|Ref set PMIDS|" + annotation_variants.size());
      System.out.print("|Total|" + total_variants);
      System.out.print("|Matched|" + gene_variants_matched);
      System.out.print("|Recall|" + recall);

      System.out.print("|Matched, only variant|" + variants_matched);
      System.out.print("|Recall, only variant|" + recall_variants);
    }
    
    {
      System.out.print("|Common PMIDs|" + common_pmids.size());
      
      double recall = (double)gene_variants_matched/total_variants_common;
      double recall_variants = (double)variants_matched/total_variants_common;
    
      System.out.print("|Total common|" + total_variants_common);
      System.out.print("|Matched|" + gene_variants_matched);
      System.out.print("|Recall|" + recall);
    
      System.out.print("|Matched, only variant|" + variants_matched);
      System.out.print("|Recall, only variant|" + recall_variants);
    }
    
    System.out.println("|PMIDS in file|" + pmids.size());

  }
}