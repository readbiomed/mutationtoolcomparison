package readbiomed.mtc.pmc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import monq.jfa.AbstractFaAction;
import monq.jfa.CallbackException;
import monq.jfa.Dfa;
import monq.jfa.DfaRun;
import monq.jfa.Nfa;
import monq.jfa.ReaderCharSource;
import monq.jfa.Xml;

/**
 * Elaboration of a list of URLs to download supplementary material files from PMC.
 * No download is done, a script is produced that uses lynx to download the files.
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class PMCDownloadSupplementaryMaterial
{
  private static Dfa dfa = null;

  private static String pmc_id = null;

  private static AbstractFaAction get_pmc_id = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{ 
      if (pmc_id == null)
      { Map <String, String> map = Xml.splitElement(yytext, start);
      
        if(map.get("pub-id-type").equals("pmc"))
        {
          pmc_id = map.get(Xml.CONTENT);
          Map <String, List <String>> pmc_file = (Map <String, List <String>>)runner.clientData;
          pmc_file.put(pmc_id, new ArrayList <String> ());
        }
      }
  	}
  };

  private static AbstractFaAction get_media = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{ 
      if (pmc_id != null)
      { Map <String, String> map = Xml.splitElement(yytext, start);

        if (map.get("xlink:href") != null)
        { ((Map <String, List <String>>)runner.clientData).get(pmc_id).add(map.get("xlink:href")); }
      }
  	}
  };

  static
  {
    try
    {     
  	  Nfa nfa = new Nfa(Nfa.NOTHING);
  	  nfa.or(Xml.GoofedElement("article-id"), get_pmc_id);
  	  nfa.or(Xml.GoofedElement("media"), get_media);
  	  nfa.or("<media (([^/]+)/>)!", get_media);
	  dfa = nfa.compile(DfaRun.UNMATCHED_COPY);
    }
    catch (Exception e)
    { throw new Error("PMCDownloadSupplementaryMaterial error!", e); }
  }

  /**
   * Example call: PMCDownloadSupplementaryMaterial [folder]
   * 
   * @param argc
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
	if (argc.length != 1)
	{
	  System.err.println("PMCDownloadSupplementaryMaterial [folder]");
	  System.exit(-1);
	}
	
    // PMC folder
	File folder = new File(argc[0]);

	Map <String, List <String>> pmc_file =
			new HashMap <String, List <String>> ();

	if (folder.isDirectory() && folder.listFiles() != null)
	{
	  for (File file : folder.listFiles())
      {
		if (file.getName().endsWith("xml"))
		{
	      // PMC file
		  // Extract supplementary material files
	      DfaRun dfaRun = new DfaRun(dfa);
	      dfaRun.clientData = pmc_file;
          dfaRun.setIn(new ReaderCharSource(new BufferedReader(new FileReader(file))));
          dfaRun.filter();

          pmc_id = null;
		}
      }
	}

    for (Map.Entry <String, List <String>> entry : pmc_file.entrySet())    
    {
      for (String file : entry.getValue())
      {
    	System.out.println("lynx -source http://www.ncbi.nlm.nih.gov/pmc/articles/PMC" + entry.getKey() + "/bin/" + file + " > " + entry.getKey() + "_" + file);
    	System.out.println("sleep 60");
      }
    }
  }
}