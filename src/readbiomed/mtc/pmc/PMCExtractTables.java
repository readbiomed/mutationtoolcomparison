package readbiomed.mtc.pmc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import monq.jfa.AbstractFaAction;
import monq.jfa.CallbackException;
import monq.jfa.Dfa;
import monq.jfa.DfaRun;
import monq.jfa.Nfa;
import monq.jfa.ReaderCharSource;
import monq.jfa.Xml;
import monq.jfa.actions.Printf;

/**
 * Extraction of table information from PMC articles.
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 * 
 */
public class PMCExtractTables
{
  private static Dfa dfa = null;
  private static Dfa dfaTable = null;
  private static Dfa dfaRow = null;
  
  private static StringBuilder sb = new StringBuilder();

  private static boolean in_body = false;
  
  private static List <String> list = new ArrayList <String> ();

  private static AbstractFaAction get_table = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{
	  DfaRun dfaRun = new DfaRun(dfaTable);

	  try
	  { dfaRun.filter(yytext.substring(start)); }
	  catch (IOException e)
	  { e.printStackTrace(); }
  	}
  };

  private static AbstractFaAction get_body_start = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{ in_body = true; }
  };

  private static AbstractFaAction get_body_end = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{
	  in_body = false;
	  
	  if (sb.length() > 0)
	  {
		list.add(sb.toString());
	    sb.setLength(0);
	  }
  	}
  };

  private static AbstractFaAction get_caption = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{ 
	    DfaRun dfaRun = new DfaRun(dfaRow);

	    try
	    { sb.append(" ").append(dfaRun.filter(yytext.substring(start).replaceAll("\r|\n|\t", " ").replaceAll("&gt;", ">").replaceAll("�", " "))); }
	    catch (IOException e)
	    { e.printStackTrace(); }
  	}
  };
  
  private static AbstractFaAction get_row = new AbstractFaAction()
  {
	public void invoke(StringBuffer yytext,int start,DfaRun runner)
  	throws CallbackException
  	{ 
	  if (in_body)
	  {
	    DfaRun dfaRun = new DfaRun(dfaRow);

	    try
	    { sb.append(" ").append(dfaRun.filter(yytext.substring(start).replaceAll("\r|\n|\t", " ").replaceAll("&gt;", ">").replaceAll("�", " "))); }
	    catch (IOException e)
	    { e.printStackTrace(); }
	  }
  	}
  };
  
  static
  {
    try
    {     
  	  Nfa nfa = new Nfa(Nfa.NOTHING);
  	  nfa.or(Xml.GoofedElement("table-wrap"), get_table);
	  dfa = nfa.compile(DfaRun.UNMATCHED_COPY);

  	  Nfa nfaTable = new Nfa(Nfa.NOTHING);
  	  nfaTable.or(Xml.GoofedElement("caption"), get_caption);
  	  nfaTable.or(Xml.STag("tbody"), get_body_start);
  	  nfaTable.or(Xml.ETag("tbody"), get_body_end);
  	  nfaTable.or(Xml.GoofedElement("tr"), get_row);
	  dfaTable = nfaTable.compile(DfaRun.UNMATCHED_COPY);
	  
  	  Nfa nfaRow = new Nfa(Nfa.NOTHING);
  	  nfaRow.or(Xml.STag(), new Printf(" "));
  	  nfaRow.or(Xml.ETag(), new Printf(" "));
  	  nfaRow.or("<(.*/>)!", new Printf(" "));
	  dfaRow = nfaRow.compile(DfaRun.UNMATCHED_COPY);
    }
    catch (Exception e)
    { throw new Error("PMCExtractTables error!", e); }
  }

  /**
   * Example call: PMCExtractTables [pmc_folder]
   * 
   * @param argc - [folder with PMC articles]
   * @throws IOException
   */
  public static void main (String [] argc) throws IOException
  {
	if (argc.length != 1)
	{
      System.err.println("PMCExtractTables [pmc_folder]");
      System.exit(-1);
	}
	  
	File folder = new File(argc[0]);
	
	System.out.println("PMID\tTitle\tAbstract");

	if (folder.isDirectory() && folder.listFiles() != null)
	{
      for (File file : folder.listFiles())
      {
        if (file.getName().endsWith(".xml"))
        {
          String pmid = file.getName().replaceAll(".xml", "");
          
          list.clear();
          sb.setLength(0);
          
          DfaRun dfaRun = new DfaRun(dfa);
          dfaRun.setIn(new ReaderCharSource(new BufferedReader(new FileReader(file))));
          dfaRun.filter();

          for (String string : list)
          { System.out.println(pmid + "\t\t" + string); }
        }
      }
	}
  }
}