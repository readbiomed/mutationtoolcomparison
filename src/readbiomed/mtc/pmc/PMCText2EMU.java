package readbiomed.mtc.pmc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Generate the file in EMU format from PMC txt files 
 * 
 * @author Antonio Jimeno Yepes (antonio.jimeno@gmail.com)
 *
 */
public class PMCText2EMU
{
  /**
   * Example call: PMCText2EMU [text_input_folder] [emu_output_folder]
   * 
   * @param argc
   * @throws IOException
   */
  public static void main (String [] argc)
  throws IOException
  {
	if (argc.length != 2)
	{
	  System.err.println("PMCText2EMU [text_input_folder] [emu_output_folder]");
	  System.exit(-1);
	}
	  
	String folder_name = argc[0];
	String output_file_name = argc[1];

	File folder = new File (folder_name);
	
	BufferedWriter w = new BufferedWriter(new FileWriter(output_file_name));
	
	w.write("PMID\ttitle\tabstract");
	w.newLine();

	for (File file : folder.listFiles())
	{
	  if (file.getName().endsWith(".txt"))
	  {
		String pmid = file.getName().replaceAll(".txt", "");
		  
		BufferedReader b = new BufferedReader(new FileReader(file));

        String line;

        String title = b.readLine();

        StringBuilder abstract_text = new StringBuilder();

        while ((line = b.readLine()) != null)
        {
          abstract_text.append(line).append(" ");
        }

        b.close();
        
        w.write(pmid + "\t" + title.replaceAll("\t", " ") + "\t" + abstract_text.toString().replaceAll("\t", " "));
        w.newLine();
	  }
	}
	
	w.close();
  }
}